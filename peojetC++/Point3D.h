#pragma once



#include "Point.h"



class Point3D : virtual public Point
{
protected:
    double _z;



public:
    Point3D() : Point()
    {
        _z = 0;
    }



    Point3D(double x, double y, double z) : Point(x, y)
    {
        _z = z;
    }



    virtual void affiche() const override
    {
        std::cout << "Point3D = " << _x << ", " << _y << ", " << _z << std::endl;
    }
};