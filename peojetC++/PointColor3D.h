#pragma once

#include "Point3D.h"
#include "PointColor.h"
#include <iostream>



class PointColor3D : public Point3D, public PointColor
{
public:
    PointColor3D(double x, double y, double z, int color)
        : Point3D(x, y, z), PointColor(x, y, color), Point(x, y)
    {
    }



    void affiche() const
    {
        std::cout << Point3D::_x << ", " << Point3D::_y << ", " << PointColor::_x << ", " << PointColor::_y;
    }
};

