#pragma once
#include <iostream>
#include <string>
using namespace std;
class Personnage
{
public:
	int HitPoints;
	int arm;	
	int buckler = 3;
	int armor = 3;
	int attackRound = 0;
	bool cancel = false;
	bool haveBuckler = false;
	bool wearArmor = false;
	void Engage(Personnage& opponent) {

		while (HitPoints > 0 && opponent.HitPoints > 0) {
			
			takeDamage(opponent);
			if (HitPoints <= 0) {
				break;
			}
			opponent.takeDamage(*this);
			if (opponent.HitPoints <= 0) {
				break;
			}


		}
		

	}
	void takeDamage(Personnage &opponent) {
		int damage = arm;
		if (damage == 12) {
			attackRound += 1;
			if (attackRound == 3)
			{
				damage = 0;
				attackRound = 0;
				return;
			}
		}

		if (opponent.haveBuckler == true && opponent.buckler > 0 && opponent.cancel == false) {
			opponent.cancel = true;
			if (damage == 6 ) {
				
				opponent.buckler -= 1;
			}
			
			
		}
		else
		{
			opponent.cancel = false;
			if (opponent.wearArmor == true)
			{
				damage -= opponent.armor;
				opponent.HitPoints -= damage;
			}
			else
			{
				opponent.HitPoints -= damage;
			}
			
		}
		
		
	};
	void Equip(const std::string& item) {
		
		if (item == "buckler") 
		{
			haveBuckler = true;

		}
		if (item == "armor")
		{
			wearArmor = true;
			arm -= 1;
		}

	};

};