#pragma once
#include <iostream>
#include "Point.h"




class PointColor : virtual public Point
{
protected:
    int _color;



public:
    PointColor(double x, double y, int color) : Point(x, y)
    {
        _color = color;
    }
};
